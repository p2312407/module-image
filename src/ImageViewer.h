#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H
#include </usr/include/SDL2/SDL.h>
#include "Image.h" 

/**
 * \class ImageViewer
 * @brief Il s'agit de la classe ImageViewer
 * */
class ImageViewer {
private:
    SDL_Window* window;
    SDL_Renderer* renderer;

public:
    
	/**
	* @brief Constructeur qui initialise tout SDL2 et crée la fenêtre
		* */
    ImageViewer();
	
    /**
	* @brief Détruit et ferme SDL2
		* */
    ~ImageViewer();

    /**
	* @brief fonction affiche image
	* @param & im
		* */
    void afficher(const Image& im);
};

#endif // IMAGEVIEWER_H

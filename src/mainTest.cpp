#include "Image.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {
Pixel p1;
Pixel p2(10,20,30);

cout<<"Le premier pixel est "<<p1.r<<" "<<p1.g<<" "<<p1.b<<endl;
cout<<"Le deuxieme pixel est "<<p2.r<<" "<<p2.g<<" "<<p2.b<<endl;

Image::testRegression();
return 0;
}

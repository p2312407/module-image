#include "Pixel.h"
#include "Image.h"
#include <iostream>
#include <cassert>
#include <fstream>
using namespace std;

// Constructeur par défaut
Image::Image(){
    dimx = 0;
    dimy = 0;
    tab = nullptr;
}

// Constructeur par copie
Image::Image(unsigned int dimensionX, unsigned int dimensionY ){
    // Vérification des dimensions
    assert(dimensionX > 0 && dimensionY > 0);
    dimx = dimensionX;
    dimy = dimensionY;
    // Allocation du tableau de pixels
    tab = new Pixel[dimx * dimy];
    // Initialisation de chaque pixel du tableau avec la couleur noire
    for (unsigned int i = 0; i < dimx * dimy; ++i) {
        tab[i] = Pixel(); 
    }
}

// Destructeur
Image::~Image() {
    if (tab != nullptr) {
        delete[] tab; // Libérer la mémoire du tableau de pixels
        tab = nullptr; 
    }
    dimx = 0;
    dimy = 0;
}

// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
// x : la coordonnée x du pixel
// y : la coordonnée y du pixel
// Retourne une référence au pixel original .
Pixel& Image::getPix(unsigned int x, unsigned int y) {
    assert(x >= 0 && x <= dimx && y >= 0 && y <= dimy);
    return tab[y * dimx + x];   
}

// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
// x : la coordonnée x du pixel
// y : la coordonnée y du pixel
// Retourne une copie du pixel original .
Pixel Image::getPix(unsigned int x, unsigned int y)  const{
    assert(x >= 0 && x <= dimx && y >= 0 && y <= dimy);
    return tab[y * dimx + x];
}

// Modifie le pixel de coordonnées (x,y) avec la nouvelle couleur 
void Image::setPix(unsigned int x, unsigned int y, Pixel couleur) {
    assert(x >= 0 && x <= dimx && y >= 0 && y <= dimy);
    tab[y * dimx + x] = couleur;
}

// Dessine un rectangle dans l'image
// Xmin, Ymin : les coordonnées du coin supérieur gauche du rectangle
// Xmax, Ymax : les coordonnées du coin inférieur droit du rectangle
// couleur : la couleur du rectangle
void Image::dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, Pixel couleur) {
    // Vérification des coordonnées
    assert(Xmin >= 0 && Ymin >= 0 && Xmax <= dimx && Ymax <= dimy);
    // Parcours de toutes les positions du rectangle 
    for (unsigned int x = Xmin; x <= Xmax; x++) {
        for (unsigned int y = Ymin; y <= Ymax; y++) {
            setPix(x, y, couleur);
        }
    }
}

// Efface l'image en la remplissant de la couleur en paramètre
void Image::effacer(Pixel couleur) {
    // Appel de la fonction dessinerRectangle avec les dimensions complètes de l'image
    dessinerRectangle(0, 0, dimx - 1, dimy - 1, couleur);
}

// Sauvegarde l'image dans un fichier filename
void Image::sauver(const string &filename) const {
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    // Parcours de tous les pixels pour les écrire dans le fichier
    for (unsigned int y = 0; y < dimy; ++y) {
        for (unsigned int x = 0; x < dimx; ++x) {
            const Pixel& pix = getPix(x, y); // Utiliser getPix(x, y) pour accéder aux pixels
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

// Ouvre un fichier filename et charge son contenu dans l'image
void Image::ouvrir(const string &filename) {
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr) delete[] tab;
    tab = new Pixel[dimx * dimy];
    // Parcours de tous les pixels pour les lire depuis le fichier
    for (unsigned int y = 0; y < dimy; ++y) {
        for (unsigned int x = 0; x < dimx; ++x) {
            fichier >> r >> b >> g;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

// Affiche les valeurs des pixels de l'image sur la console
void Image::afficherConsole() {
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y) {
        for (unsigned int x = 0; x < dimx; ++x) {
            Pixel &pix = getPix(x, y); //accéder aux pixels avec des appels getPix(x, y)
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}

void Image::testRegression() {
    // Test du constructeur par défaut
    Image image;
    assert(image.dimx == 0); 
    assert(image.dimy == 0); // Vérifie que les dimensions X et Y de l'image sont 0
    assert(image.tab == nullptr); // Vérifie que le tableau de pixels est nul

    // Test du constructeur avec copie
    Image image2(5, 5); // Crée une nouvelle image de dimension 5x5
    assert(image2.dimx == 5); 
    assert(image2.dimy == 5); // Vérifie que les dimensions X et Y de l'image sont 5
    assert(image2.tab != nullptr); // Vérifie que le tableau de pixels n'est pas nul

    // Vérifie que tous les pixels de l'image 2 sont initialisés à la couleur noire (0, 0, 0)
    for (unsigned int x = 0; x < image2.dimx; x++) {
        for (unsigned int y = 0; y < image2.dimy; y++) {
            assert(image2.tab[y * image2.dimx + x].r == 0); 
            assert(image2.tab[y * image2.dimx + x].g == 0); 
            assert(image2.tab[y * image2.dimx + x].b == 0); // Vérifie les composants du pixel
        }
    }

    // Test de la fonction setPix et getPix
    Pixel pixel(250, 128, 114); 
    image2.setPix(3, 3, pixel); // Définit le pixel 
    Pixel p = image2.getPix(3, 3); // Récupère le pixel
    assert(p.r == 250); 
    assert(p.g == 128); 
    assert(p.b == 114); // Vérifie les composants du pixel

    // Test de la fonction dessinerRectangle
    Pixel pixel2(255, 255, 0); 
    image2.dessinerRectangle(2, 2, 3, 3, pixel2); // Dessine un rectangle jaune aux coordonnées spécifiées et la verifie
    for (unsigned int x = 2; x <= 3; x++) {
        for (unsigned int y = 2; y <= 3; y++) {
            assert(image2.tab[y * image2.dimx + x].r == 255); 
            assert(image2.tab[y * image2.dimx + x].g == 255);
            assert(image2.tab[y * image2.dimx + x].b == 0);  // Vérifie les composants du pixel
        }
    }

    // Test de la fonction effacer
    image2.effacer(Pixel(0, 0, 0)); // Efface l'image en la remplissant de noir et la verifie
    for (unsigned int x = 0; x < image2.dimx; x++) {
        for (unsigned int y = 0; y < image2.dimy; y++) {
            assert(image2.tab[y * image2.dimx + x].r == 0); 
            assert(image2.tab[y * image2.dimx + x].g == 0); 
            assert(image2.tab[y * image2.dimx + x].b == 0); // Vérifie les composants du pixel
        }
    }
     cout << "Tests de regression réussis !" << endl;
}

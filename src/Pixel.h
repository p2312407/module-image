#ifndef _PIXEL_H
#define _PIXEL_H
/**
 * \struct Pixel
 * @brief Il s'agit de la stucteur Pixel
 * */
struct Pixel {
    
	unsigned char r;
	unsigned char g;
    unsigned char b;


    /**
	* @brief Constructeur par defaut
		* */
    Pixel(){r=g=b=0;}

    /**
	* @brief Constructeur qui prend 3 parametres 
	* @param nr
	* @param ng
    * @param nb
		* */
    Pixel(unsigned char  nr, unsigned char ng, unsigned char nb){r=nr;g=ng;b=nb;}
};
#endif



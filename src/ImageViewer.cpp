#include <iostream>
#include <SDL_image.h> 
#include "ImageViewer.h"

// Constructeur de ImageViewer
ImageViewer::ImageViewer() {
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow("ImageViewer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 510, 510, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255); 
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
}

// Destructeur de la classe ImageViewer
ImageViewer::~ImageViewer() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

//Afficher l'image dans la fenêtre SDL2
void ImageViewer::afficher(const Image& im)  {
	    SDL_Surface* surface = SDL_CreateRGBSurfaceFrom(
		im.tab,          
		im.dimx,         
		im.dimy,         
		24,              
		im.dimx * 3,    
		0x0000FF,  
        0x00FF00,  
        0xFF0000, 
        0);

    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    SDL_RenderCopy(renderer, texture, nullptr, nullptr);
    SDL_RenderPresent(renderer);

    SDL_Event event;
    while (true) {
        SDL_PollEvent(&event);
        if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)) {
            break;
        }
    }

    // Destruction
    SDL_DestroyTexture(texture);
}

#ifndef _IMAGE_H
#define _IMAGE_H

#include "Pixel.h"
#include <string>
using namespace std;
/**
 * \class Image
 * @brief Il s'agit de la classe Image
 * */
class Image {
private:
    Pixel *tab; // Tableau de pixels
    unsigned int dimx, dimy; // Dimensions de l'image

public:
    /**
	* @brief Constructeur par defaut
		* */
    Image();
    
    /**
	* @brief Constructeur qui prend deux parametre
	* @param dimensionX 
	* @param dimensionY
		* */
    Image(unsigned int dimensionX, unsigned int dimensionY);
 
    /**
	* @brief Destructeur
		* */
    ~Image();
    

    /**
	* @brief Récupère le pixel original de coordonnées (x,y) et renvoie l'original
	* @param x
	* @param y
		* */
    Pixel& getPix(unsigned int x, unsigned int y);
  
    /**
	* @brief Récupère le pixel original de coordonnées (x,y) et renvoie une copie.
	* @param x
	* @param y
		* */
    Pixel getPix(unsigned int x, unsigned int y) const;
    
   
    /**
	* @brief Modifie le pixel de coordonnées (x,y)
	* @param x
	* @param y
    * @param couleur
		* */
    void setPix(unsigned int x, unsigned int y, Pixel couleur);
  
    /**
	* @brief Dessine un rectangle plein de la couleur dans l'image
	* @param Xmin
	* @param Ymin
	* @param Xmax
	* @param Ymax
    * @param couleur
		* */
    void dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, Pixel couleur);
  
  
    /**
	* @brief Efface l'image en la remplissant de la couleur en paramètre
    * @param couleur
		* */
    void effacer(Pixel couleur);
    
    /**
	* @brief Effectue une série de tests vérifiant que toutes les fonctions fonctionnent correctement
		* */
    static void testRegression();
    
    /**
	* @brief Sauvegarde l'image dans un fichier au format PPM
    * @param &filename
		* */
    void sauver(const string &filename) const;
    
    
     /**
	* @brief Ouvre une image à partir d'un fichier au format PPM
    * @param &filename
		* */
    void ouvrir(const string &filename);
    
    
     /**
	* @brief Affiche l'image dans la console
		* */
    void afficherConsole();
    
    friend class ImageViewer ;
};
#endif

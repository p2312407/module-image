all :./bin/test ./bin/exemple ./bin/affichage

./bin/affichage: ./obj/mainAffichage.o ./obj/ImageViewer.o ./obj/Image.o
	g++ ./obj/mainAffichage.o ./obj/ImageViewer.o ./obj/Image.o -o ./bin/affichage $(shell sdl2-config --cflags --libs)

./bin/test: ./obj/mainTest.o ./obj/Image.o
	g++ ./obj/mainTest.o ./obj/Image.o -o ./bin/test 

./bin/exemple: ./obj/mainExemple.o ./obj/Image.o
	g++ ./obj/mainExemple.o ./src/Pixel.h ./obj/Image.o -o ./bin/exemple
	
./obj/mainExemple.o: ./src/Image.h ./src/mainExemple.cpp
	g++ -g -c ./src/mainExemple.cpp -o ./obj/mainExemple.o

./obj/mainAffichage.o:./src/ImageViewer.h ./src/mainAffichage.cpp ./src/Image.h
	g++ -g -c ./src/mainAffichage.cpp -o ./obj/mainAffichage.o  $(shell sdl2-config --cflags)

./obj/mainTest.o: ./src/Image.h ./src/Pixel.h ./src/mainTest.cpp
	g++ -g -c ./src/mainTest.cpp -o ./obj/mainTest.o
	
./obj/Image.o: ./src/Image.h ./src/Image.cpp ./src/Pixel.h
	g++ -g -c ./src/Image.cpp -o ./obj/Image.o $(shell sdl2-config --cflags)
	

./obj/ImageViewer.o:./src/ImageViewer.h ./src/ImageViewer.cpp ./src/Image.h
	g++ -g -c ./src/ImageViewer.cpp -o ./obj/ImageViewer.o $(shell sdl2-config --cflags)
	
doc: doc/doxyfile
	doxygen doc/doxyfile
	
clean:
	rm ./obj/* ./bin/* ./data/*
